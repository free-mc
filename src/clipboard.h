
/** \file  clipboard.h
 *  \brief Header: Util for external clipboard
 */

#ifndef MC_CLIPBOARD_H
#define MC_CLIPBOARD_H

gboolean copy_file_to_ext_clip (void);
gboolean paste_to_file_from_ext_clip (void);

#endif
