
/** \file option.h
 *  \brief Header: configure box module
 */

#ifndef MC_OPTION_H
#define MC_OPTION_H

void configure_box (void);
void panel_options_box (void);

#endif
